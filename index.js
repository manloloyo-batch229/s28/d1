//MongoDb cloud server platform

        // database user
        //username:admin
        //password:admin1234



//db.users.insertOne({}) / inserting one document
//db.users.insertMany([]) / inserting multiple document
//db.collections.find() -find all collections

                // CREATE
db.users.insertOne({
        
        "username": "dahyunKim",
        "password": "once1234"
    
    
    })
    
db.users.insertOne({
        
        "username": "liester",
        "password": "liester1234"
    
    
    })

//db.users.insertMany([]) / inserting multiple document
db.users.insertMany([

    {
        "username": "pablo123",
        "password": "123paul"
    },
    {
        "username": "pedro09",
        "password": "iampeter99"
    }


])

// Commands for creating documents:
/*
        db.collection.insertOne({
                "field1": "value",
                "field2": "value"

        })

        db.collection.insertMany({
                "field1": "valueA",
                "field2": "valueB"

        })

*/

                // READ

db.products.find()

db.users.find({"username":"pedro09"})

db.cars.insertMany([
        {
            name: "Vios",
            brand: "Toyota",
            type: "sedan",
            price: 1500000
        },
        {
            name: "Tamaraw FX",
            brand: "Toyota",
            type: "auv",
            price: 750000
        },
        {
            name: "City",
            brand: "Honda",
            type: "sedan",
            price: 1600000
        }

])
// Mini-Activity
//Retrieve/find all car document
//Retrive/find all car document which type is "sedan"

db.cars.find()

db.cars.find({type:"sedan"})

// find() -gets all documents that matches the criteria

//db.cars.findOne({}) -gets the first document that matches the criteria
db.cars.findOne({type: "sedan"}) // the first match ang pinipili

        // UPDATE

//update or change the value from pedro to peter

//updateOne() - allows us to update the first item that matches the criteria

db.users.updateOne({username:"pedro09"},{$set:{username:"peter1999"}})

// db.users.updateOne({username:"pablo123"},{$set:{isAdmin:true}}) - di naka initialize and because of the updateOne nag add siya ng bagong document sa loob ni pablo123

// updateMany() - allows us to update all documents that matches the criteria
//db.users.updateMany({},{$set:{isAdmin:true}})
//db.cars.updateMany({type:"sedan"},{$set:{price:1000000}})


                // DELETE
//deleteOne() - deletes the first document that matches the criteria
//db.users.deleteOne({})
//db.cars.deleteOne({brand: "Toyota"})

//deleteMany() - deletes all items that matches the criteria
//db.users.deleteMany({isAdmin:true})

//db.cars.deleteMany({}) - delete all
